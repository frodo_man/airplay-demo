//
//  main.m
//  AirplayDemo
//
//  Created by XH.Liu on 05/09/2016.
//  Copyright © 2016 Xmartsoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

//
//  AppDelegate.h
//  AirplayDemo
//
//  Created by XH.Liu on 05/09/2016.
//  Copyright © 2016 Xmartsoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


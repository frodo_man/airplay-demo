//
//  ViewController.m
//  AirplayDemo
//
//  Created by XH.Liu on 05/09/2016.
//  Copyright © 2016 Xmartsoft. All rights reserved.
//

#import "ViewController.h"
#import <MediaPlayer/MediaPlayer.h>

@interface ViewController ()
@property (strong, nonatomic) UIPopoverController *airplayPopoverController;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)buttonAirplayTapped:(UIButton*)sender
{
    if(self.airplayPopoverController == nil)
    {
        //create the MPAudioVideoRoutingPopoverController class at runtime
        id TheClass = NSClassFromString(@"MPAudioVideoRoutingPopoverController");
        
        // a temp object of MPAudioVideoRoutingPopoverController
        id testObject = [[TheClass alloc] initWithContentViewController: self];
        // the selector to call for creating the right popover controller
        SEL initMethod = @selector(initWithType:includeMirroring:);
        
        if([testObject respondsToSelector: initMethod])
        {
            //use NSInvocation to create the object we need.
            NSMethodSignature* signature = [TheClass instanceMethodSignatureForSelector: initMethod];
            NSInvocation* invocation = [NSInvocation invocationWithMethodSignature: signature];
            [invocation setTarget: testObject];
            [invocation setSelector:initMethod];
            
            //prepare arguments
            NSInteger type = 0;
            BOOL showMirroring = YES;
            
            //the final popover controller
            static id returnObject = nil;
            
            // index 0 and 1 are reserved for system use: self; _cmd;
            // call 'initWithType:0 includeMirroring:YES' in runtime
            [invocation setArgument:&type atIndex:2];
            [invocation setArgument:&showMirroring atIndex:3];
            [invocation retainArguments];
            [invocation invoke];
            [invocation getReturnValue: &returnObject];
            
            //save the popover controller as a property
            self.airplayPopoverController = returnObject;
            
        }
    }
    //Dispaly the popover controller
    [self.airplayPopoverController presentPopoverFromRect:sender.frame
                                                   inView:self.view
                                 permittedArrowDirections:UIPopoverArrowDirectionAny
                                                 animated:YES];
}

@end
